#!/bin/sh
# [Gedit Tool]
# Input=selection-document
# Save-files=nothing
# Name=SED
# Applicability=all
# Output=replace-selection


re_memory="$HOME/.config/last-sed-re-for-gedit"

re="$(
zenity --entry --title="SED" \
--text="O comando sed será executado recebendo a seleção atual e a substituirá pelo seu resultado.

Insira a expressão regular:" \
--entry-text="$(cat $re_memory 2>/dev/null)"
)"

if test -n "$re"; then
  echo "$re" > $re_memory
  sed -r "$re"
else
  cat # return the same value
fi
